import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseDetailsComponent } from './components/course-details/course-details.component';
import { CourseEditComponent } from './components/course-edit/course-edit.component';
import { CoursesListComponent } from './components/courses-list/courses-list.component';

const routes: Routes = [
  {
    path: 'list',
    component: CoursesListComponent
  },
  {
    path: 'details/:id',
    component: CourseDetailsComponent
  },
  {
    path: 'create',
    component: CourseEditComponent
  },
  {
    path: 'edit/:id',
    component: CourseEditComponent
  },
  {
    path: '',
    redirectTo: 'list'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule {
}
