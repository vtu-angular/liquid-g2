import { Course } from '../../courses/models/course.model';

export class Category {
  id: number;
  created: Date;
  lastUpdated: Date;

  name: string;

  courses: Course[];
}
