import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesByCategoriesReportComponent } from './courses-by-categories-report.component';

describe('CoursesByCategoriesReportComponent', () => {
  let component: CoursesByCategoriesReportComponent;
  let fixture: ComponentFixture<CoursesByCategoriesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoursesByCategoriesReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesByCategoriesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
