import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { CoursesService } from '../../../courses/services/courses.service';

@Component({
  selector: 'lq-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  data = [];

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  gradient = false;
  isDoughnut = true;

  constructor(private coursesService: CoursesService) {
  }

  ngOnInit(): void {
    this.coursesService.getAll$().pipe(
      take(1)
    ).subscribe((courses) => {
      let certified = 0;
      let notCertified = 0;

      for (const course of courses) {
        if (course.isCertificateIncluded) {
          certified++;
        } else {
          notCertified++;
        }
      }

      this.data = [
        {
          name: 'Certified',
          value: certified
        },
        {
          name: 'Not certified',
          value: notCertified
        }
      ];
    });
  }

}
