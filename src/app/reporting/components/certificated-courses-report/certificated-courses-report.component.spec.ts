import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificatedCoursesReportComponent } from './certificated-courses-report.component';

describe('CertificatedCoursesReportComponent', () => {
  let component: CertificatedCoursesReportComponent;
  let fixture: ComponentFixture<CertificatedCoursesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CertificatedCoursesReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificatedCoursesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
